// ============================================================================
//
// = CONTEXT
//   MachineStatus : monitor machine subsystems
//
// = File
//   InnerAppender.h
//
// = AUTHOR
//   Julien Malik - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iomanip>
#include <time.h>
#include <yat/threading/Mutex.h>
#include <InnerAppender.h>

// ============================================================================
// CONSTANTs
// ============================================================================
#define kLOG_NUMBER_LIMIT 2048

namespace GammaDosimeter_ns
{

// ============================================================================
// InnerAppender::InnerAppender
// ============================================================================
InnerAppender::InnerAppender (const std::string& _name, bool _open_connection)
  : log4tango::Appender(_name)
{
  if ( _open_connection )
    this->reopen();
}

// ============================================================================
// InnerAppender::~InnerAppender
// ============================================================================
InnerAppender::~InnerAppender ()
{
  this->close();
}

// ============================================================================
// InnerAppender::requires_layout
// ============================================================================
bool InnerAppender::requires_layout () const
{
  return false; 
}

// ============================================================================
// InnerAppender::set_layout
// ============================================================================
void InnerAppender::set_layout (log4tango::Layout*)
{
  // no-op
}

// ============================================================================
// InnerAppender::is_valid
// ============================================================================
bool InnerAppender::is_valid (void) const
{
  return true;
}

// ============================================================================
// InnerAppender::
// ============================================================================
int InnerAppender::_append (const log4tango::LoggingEvent& event)
{
  //------------------------------------------------------------
  //- DO NOT LOG FROM THIS METHOD !!!
  //------------------------------------------------------------

  static const size_t max_time_str_len = 32;
  
  try
  {
    //- reformat LoggingEvent
    time_t raw_time;
    ::time(&raw_time);
    char c_date_string[max_time_str_len];
    struct tm * time_info = ::localtime(&raw_time);
    ::strftime (c_date_string, max_time_str_len,"%d:%m:%y@%H:%M:%S", time_info);
  
    std::ostringstream oss;
    oss << c_date_string
        << ": ["
        << log4tango::Level::get_name(event.level)
        << "] "
        << event.message;
        
    //- push the log into the repository
    {
      yat::MutexLock scoped_lock(m_mutex);
      this->m_logs_list.push_back(oss.str());
      if (this->m_logs_list.size() > kLOG_NUMBER_LIMIT)
        this->m_logs_list.pop_front();
    }
  }
  catch (...) 
  {
    close();
    return -1;
  }
  return 0;
} 

// ============================================================================
// InnerAppender::reopen
// ============================================================================
bool InnerAppender::reopen (void) 
{
  return true;
}

// ============================================================================
// InnerAppender::close
// ============================================================================
void InnerAppender::close (void)
{
  //- noop
}
  
// ============================================================================
// InnerAppender::get_logs
// ============================================================================
void InnerAppender::get_logs (InnerAppender::LogList& log_list_)
{
  yat::MutexLock scoped_lock(m_mutex);
  log_list_ = this->m_logs_list;
}

} // namespace GammaDosimeter_ns
