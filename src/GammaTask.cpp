// ============================================================================
//
// = CONTEXT
//		TANGO Project - Task is a counter to automatically lock the Reset command !
//
// = File
//		GammaTask.cpp
//
// = AUTHOR
//		X. Elattaoui from Nicolas Leclercq - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <yat/threading/Mutex.h>
#include "GammaTask.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kUNLOCK_CMDs_MSG (yat::FIRST_USER_MSG + 1000)
#define kLOCK_CMDS_AFTER  15000           //- lock cmds after 15s

namespace GammaDosimeter_ns
{
	// ======================================================================
	// GammaTask::GammaTask
	// ======================================================================
  GammaTask::GammaTask (size_t _periodic_timeout_ms, Tango::DeviceImpl * _host_device)
    : yat4tango::DeviceTask(_host_device),
    _cmds_unlocked(false),
    _pwdInClear(""),
    _pwdDecrypted(""),
    _kn(0),
    _kd(0)
	{
    //- configure optional msg handling
    this->enable_timeout_msg(false);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(_periodic_timeout_ms);
	}

	// ======================================================================
	// GammaTask::~GammaTask
	// ======================================================================
	GammaTask::~GammaTask (void)
	{
    //- noop
	}

	// ============================================================================
	// GammaTask::process_message
	// ============================================================================
	void GammaTask::process_message (yat::Message& _msg) throw (Tango::DevFailed)
	{
	  DEBUG_STREAM << "GammaTask::handle_message::receiving msg " << _msg.to_string() << std::endl;

	  //- handle msg
    switch (_msg.type())
	  {
	    //- THREAD_INIT ----------------------
	    case yat::TASK_INIT:
	      {
  	      DEBUG_STREAM << "GammaTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;

  	      //- "initialization" code goes here
          //----------------------------------------------------

        } 
		    break;
        
		  //- THREAD_EXIT ----------------------
		  case yat::TASK_EXIT:
		    {
  			  DEBUG_STREAM << "GammaTask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;

  			  //- "release" code goes here
          //----------------------------------------------------
          
        }
			  break;
        
		  //- THREAD_PERIODIC ------------------
		  case yat::TASK_PERIODIC:
		    {
  		    DEBUG_STREAM << "GammaTask::handle_message::handling THREAD_PERIODIC msg" << std::endl;

          //- code relative to the task's periodic job goes here
          //----------------------------------------------------
          
		      { //- enter critical section
            yat::AutoMutex<> guard(this->m_lock);

            if ( this->_timer.elapsed_msec() > kLOCK_CMDS_AFTER )
              this->_cmds_unlocked = false;

          } //- leave critical section
        }
		    break;
        
		  //- THREAD_TIMEOUT -------------------
		  case yat::TASK_TIMEOUT:
		    {
  		    //- code relative to the task's tmo handling goes here

  		    DEBUG_STREAM << "GammaTask::handle_message::handling THREAD_TIMEOUT msg" << std::endl;
        }
		    break;
        
		  //- USER_DEFINED_MSG -----------------
		  case kUNLOCK_CMDs_MSG:
		    {
  		  	DEBUG_STREAM << "GammaTask::handle_message::handling kUNLOCK_CMDs_MSG user msg" << std::endl;

          //- decode crypted list of codes
          this->decrypt_codes();

          //- compare passwords and unlock cmds if OK
          this->unlock_cmds();

          //- restart timer
          if ( this->_cmds_unlocked )
          {
            //- restart timer
            this->_timer.restart();
          }

          //- dump the received value
          std::cout << "GammaTask::handle_message:: is commands unlocked : " 
                       << this->_cmds_unlocked 
                       << std::endl;
  		  }
  		  break;
        
      //- UNHANDLED MSG --------------------
  		default:
  		  DEBUG_STREAM << "GammaTask::handle_message::unhandled msg type received" << std::endl;
  			break;
	  }

	  DEBUG_STREAM << "GammaTask::handle_message::message_handler:msg " 
                 << _msg.to_string() 
                 << " successfully handled" 
                 << std::endl;
  }

	// ============================================================================
	// GammaTask::decrypt_codes
	// ============================================================================
	void GammaTask::decrypt_codes ()
	{
	  char l;
    std::string mdpResult("");
	std::cout << "GammaTask::decrypt_codes ..." << std::endl;
	std::cout << "Key n : " << this->_kn << std::endl;
	std::cout << "Key d : " << this->_kd << std::endl;
	std::cout << "sizeof(unsigned long int) = " << sizeof(unsigned long int) << std::endl;
    for (unsigned long int idx = 0; idx < this->_codesList.size(); ++idx)
    {
		std::cout << " code to be decrypted : " << _codesList.at(idx) << std::endl;
      l = decrypt(_codesList.at(idx));
      std::cout << "idx = " << idx << "\t char = \'" << l << "\'" << std::endl;
      mdpResult += l;
    }

    this->_pwdDecrypted = mdpResult;
    std::cout << "\n FIN DECRYPTAGE : " << mdpResult.c_str() << std::endl;
  }

  /****************************FONCTION DECRYPTAGE*******************************/
  unsigned char GammaTask::decrypt(unsigned long int k)
  {
    unsigned long int i,res = 1;
    unsigned char x;

    for(i = 0; i < this->_kd; i++)
    {
      res = res * k;
      res = fmod((double)res,(double)this->_kn);
    }

    x = ((unsigned char)res);

    return x;
  }

  // ============================================================================
	// GammaTask::unlock_cmds
	// ============================================================================
	void GammaTask::unlock_cmds ()
	{
    //- compare returns 0 if strings are equals
    if ( !this->_pwdInClear.compare(this->_pwdDecrypted) )
      this->_cmds_unlocked = true;
  }

} // namespace
