// ============================================================================
//
// = CONTEXT
//		GammaTask
//
// = File
//		GammaTask.h
//
// = AUTHOR
//		X. Elattaoui from Nicolas Leclercq - SOLEIL
//
// ============================================================================

#ifndef _GAMMA_TASK_H_
#define _GAMMA_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <vector>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>        //- timer to check if motor motion is started

namespace GammaDosimeter_ns
{

// ============================================================================
// class: GammaTask
// ============================================================================
class GammaTask : public yat4tango::DeviceTask
{
public:
	//- ctor ---------------------------------
	GammaTask (size_t _periodic_timeout_ms, Tango::DeviceImpl * _host_device);

	//- dtor ---------------------------------
	virtual ~GammaTask (void);

  inline void set_password(const std::string& pwd) {
    this->_pwdInClear = pwd;
  }

  inline void set_n (unsigned long int n) {
    this->_kn = n;
  }

  inline void set_d (unsigned long int d) {
    this->_kd = d;
  }

  inline void set_codesList (std::vector<unsigned long int>& codesList) {
    this->_codesList.assign(codesList.begin(), codesList.end());
	std::cout << "set_codesList -> DONE." << std::endl;
  }

  inline bool is_cmds_unlocked () {
    return this->_cmds_unlocked;
  }

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);

  void decrypt_codes (void);
private:
  unsigned char decrypt(unsigned long int k);
	void unlock_cmds (void);

  //- is commands availables
  bool _cmds_unlocked;
  //- password
  std::string _pwdInClear;
  std::string _pwdDecrypted;
  //- keys
  unsigned long int _kn;
  unsigned long int _kd;
  //- codes list to decrypt
  std::vector<unsigned long int> _codesList;

  yat::Timer _timer;
  
  //- Utilities
  yat::Mutex m_lock;

};

} // namespace GammaDosimeter_ns

#endif // _GAMMA_TASK_H_
