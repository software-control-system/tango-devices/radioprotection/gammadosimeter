import sys

from time import sleep, localtime, strftime
import pickle # used to write data
from PyTango import *

# Device name 
tangotest  = ["ANS-C01/RP/CIG.012","ANS-C01/RP/CIG.031","ANS-C15/RP/CIG.029","ANS-C16/RP/CIG.010","ANS-C16/RP/CIG.011",
              "ANS-C16/RP/CIG.030","BOO-C04/RP/CIG.004","BOO-C05/RP/CIG.005","BOO-C13/RP/CIG.009","BOO-C16/RP/CIG.007",
              "BOO-C17/RP/CIG.008","BOO-C21/RP/CIG.003","ANS-C13/RP/CIG.027","ANS-C14/RP/CIG.028","LIN/RP/CIG.001","LT1/RP/CIG.002"]
#  Protect the script from Exceptions raised by the Tango or python itself
try :
        # Open the file to write data
        fic = open("RecordGammaValues.txt", 'a')
        columStr = "Timestamp\t, "
        print columStr, 
        fic.write(columStr)

        for i in range (0, 16):
            columStr =  tangotest[i]+"/dose,\t" + tangotest[i]+"/doseRate,\t" ;  
             #pickle.dump(columStr, fic)
            print columStr,
            fic.write(columStr)

        print "\n"
        fic.write("\n")
        while 1:
            heure= strftime("%a, %d %b %Y %H:%M:%S +0000", localtime())
            print heure ,
            fic.write(heure)
            for i in range (0, 16):
                # Get proxy on the GammaDosimeter devices
                #print "Getting DeviceProxy "
                try :
                    proxy = DeviceProxy(tangotest[i])

                    #Read a scalar attribute
                    dose=proxy.read_attribute("dose")
                    #print "attribute value", dose.value
                    #Read a spectrum attribute
                    doseRate=proxy.read_attribute("doseRate")
                    #print "attribute value", doseRate.value
                    #Read a spectrum attribute
                    timeStamp=proxy.read_attribute("timeStamp")
                    #print "attribute value", dose.value

                    # Write data
                    strLine =   "," + str(dose.value) + "," + str(doseRate.value)
                    print strLine,
                    fic.write(strLine)
                except :
                    strLine =   ",-9999, -9999"
                    print strLine,
                    fic.write(strLine)
                
                # quand tous les moniteurs sont ecrits on passe a la ligne 
           # on attend pour l'acquisition suivante 
            print "\n"
            fic.write("\n")
            fic.flush()
            sleep(10)
           
           #pickle.dump(strLine, fic)
#	Catch Tango and Systems  Exceptions
except DevFailed:
	exctype , value = sys.exc_info()[:2]
	print "Failed with exception !", exctype
	for err in value :
		print "---ERROR ELEMENT-------"
		print "reason:" , err['reason']
		print "description:" , err["desc"]
		print "origin:" , err["origin"]
		print "severity:" , err["severity"]
	
